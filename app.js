const express = require('express');
const path = require('path');




const app = express();



app.use(express.static(path.join(__dirname, '/public')))

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'))


app.get('/', (req, res) => {
  res.redirect('/brief.html')
})

app.get('/game', async (req, res) => {
  res.render("game.ejs",{pageName:"game"})
})

app.get('/test', (req, res) => {
  res.render('test.ejs')
})




app.listen(3000, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Listening on port: 3000, YAYYY !');
  }
})