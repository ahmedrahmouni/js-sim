export class Person {
  state;
  color;
  constructor(canvas, personRadius, state) {
    let maxx = canvas.width - personRadius - 2
    let minx = personRadius + 2
    let maxy = canvas.height - personRadius - 2
    let miny = personRadius + 2
    
    this.xpos = Math.floor(Math.random()*(maxx - minx)+ minx) ;
    this.ypos = Math.floor(Math.random()*(maxy - miny)+ miny) ;
    this.state = state; // 0 healthy , 1 infected, 2 dead,   3 recovered  
    this.color = state == 0 ? imgHealhty : imgInfected ;
    
    this.xstep = Math.floor(Math.random()*3-3) ;
    this.ystep = Math.floor(Math.random()*3-3) ;
  }
  get color(){
    return this.color ;
  }

  get state() {
    return this.state ;
  }
  get position() {
    return {'x':this.xpos ,'y':this.ypos};
  }
  get direction() {
    return {'x':this.xstep ,'y':this.ystep};
  }

  setX(x) { 
    this.xpos = x ; 
  }
  setY(y) { 
    this.ypos = y ; 
  }
  setDirX(x) { 
    this.xstep = x ; 
  }
  setDirY(y) { 
    this.ystep = y ; 
  }
  isInfected() { 
    this.state = 1 ;
    this.color = imgInfected ;
  }
  isDead() { 
    this.state = 2 ;
    this.color = imgDead ;
    this.xstep = 0 ;
    this.ystep = 0 ;
  }
  isRecovered() { 
    this.state = 3 ;
    this.color = imgrecovered ;
  }
}
