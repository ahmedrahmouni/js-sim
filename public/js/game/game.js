
// import {  } from "engine";
var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

canvas.height = window.innerHeight*0.9 ;



const scoreInfected = document.querySelector('#infected') ;
const scoreHealthy = document.querySelector('#healthy') ;
const scoreDead = document.querySelector('#dead') ;
const scoreRecovered = document.querySelector('#recovered') ;



const settingPopul = document.getElementById("popul").oninput = function () {
  var val = document.getElementById("popul").value
  document.getElementById("populOut").innerHTML = val
}
const settingInfect = document.getElementById("infecProb").oninput = function () {
  var val = document.getElementById("infecProb").value
  document.getElementById("infecOut").innerHTML = val
}
const settingDying = document.getElementById("dyingProb").oninput = function () {
  var val = document.getElementById("dyingProb").value
  document.getElementById("dyingOut").innerHTML = val
}
const settingRecov = document.getElementById("recovProb").oninput = function () {
  var val = document.getElementById("recovProb").value
  document.getElementById("recovOut").innerHTML = val
}
const settingRadius = document.getElementById("ballRadius").oninput = function () {
  var val = document.getElementById("ballRadius").value
  document.getElementById("radiusOut").innerHTML = val
}



var imgHealhty = new Image();
var imgInfected = new Image();
var imgrecovered  = new Image();
var imgDead  = new Image();

var personRadius = 1;

// Lists for score board
let population = [] ;
let infected = new Set() ;
let healthy = new Set() ;
let dead = new Set() ;
let recovered = new Set();

numberOfPeople = document.getElementById("popul").value ; 
infectprob = document.getElementById("infecProb").value ;
dieprob = document.getElementById("dyingProb").value ;
recovprob = document.getElementById("recovProb").value ;

// Probability of getting infected
function infectionProb() {
  if (Math.random()*100 <= infectprob )
    return true ;
  else 
    return false ;
} 

function dyingProb() {
  if (Math.random()*100 <= dieprob/100 )
    return true ;
  else 
    return false ;
} 
function recoveryProb() {
  if (Math.random()*100 <= recovprob/100)
    return true ;
  else 
    return false ;
}





class Person {
  state;
  color;
  stoped;
  constructor(state) {
    let maxx = canvas.width - personRadius - 2
    let minx = personRadius + 2
    let maxy = canvas.height - personRadius - 2
    let miny = personRadius + 2
    
    this.xpos = Math.floor(Math.random()*(maxx - minx)+ minx) ;
    this.ypos = Math.floor(Math.random()*(maxy - miny)+ miny) ;
    this.state = state; // 0 healthy , 1 infected, 2 dead,   3 recovered  
    this.color = state == 0 ? imgHealhty : imgInfected ;
    
    this.xstep = Math.floor(Math.random()*(4+4)-4) ;
    this.ystep = Math.floor(Math.random()*(4+4)-4) ;
  }
  get color(){
    return this.color ;
  }

  get state() {
    return this.state ;
  }
  get position() {
    return {'x':this.xpos ,'y':this.ypos};
  }
  get direction() {
    return {'x':this.xstep ,'y':this.ystep};
  }

  setX(x) { 
    this.xpos = x ; 
  }
  setY(y) { 
    this.ypos = y ; 
  }
  setDirX(x) { 
    this.xstep = x ; 
  }
  setDirY(y) { 
    this.ystep = y ; 
  }
  isInfected() { 
    this.state = 1 ;
    this.color = imgInfected ;
  }
  isDead() { 
    this.state = 2 ;
    this.color = imgDead ;
    this.xstep = 0 ;
    this.ystep = 0 ;
  }
  isRecovered() { 
    this.state = 3 ;
    this.color = imgrecovered ;
  }
}


// Creates all the people
function createPopulation() {
  for (let i= 0; i< numberOfPeople-1; i++) { 
    x = new Person(0) ;
    population.push(x);
    healthy.add(x) 
  }
  y = new Person(1) ;
  population.push(y) ;
  infected.add(y) ;

}


function avoidWalls(i) {
  curPos = i.position ;
  curDir = i.direction ;
  if(curPos.y + curDir.y >= canvas.height - personRadius || curPos.y + curDir.y <= personRadius) {
    i.setDirY(-curDir.y) ;
  }
  if(curPos.x> canvas.width - personRadius || curPos.x + curDir.x < personRadius) {
    i.setDirX(-curDir.x) ;
  }
}


function avoidCollision(i) {
  curPos = i.position ;
  curDir = i.direction ;

  for(let element of population) {
    ax = curPos.x + curDir.x ;
    ay = curPos.y + curDir.y ;
    bx = element.position.x + element.direction.x ; 
    by = element.position.y + element.direction.y ;

    dx = bx - ax ;
    dy = by - ay ;
    distance = Math.sqrt(dx*dx + dy*dy) ;

    if (i!=element &&( distance <= 2*personRadius) ) {
      i.setDirX(-curDir.x) ;
      // i.setDirY(-curDir.y) ;
      // console.log(i.direction)
      // console.log(distance);
      // break;
    }
  };

}





//******************************* Drawing functions *******************************

function updateScore() {
  scoreInfected.innerHTML = infected.size
  scoreHealthy.innerHTML = healthy.size
  scoreDead.innerHTML = dead.size
  scoreRecovered.innerHTML = recovered.size
}

function drawPerson(person) {
  ctx.beginPath();
  ctx.arc(person.xpos,person.ypos, personRadius, 0, Math.PI*2);
  ctx.fillStyle = person.color ;
  if (person.state == 0 ) {
    ctx.fillStyle = "#00FF00";
  }
  if (person.state == 1 ) {
    ctx.fillStyle = "#FF0000";
  }
  if (person.state == 2 ) {
    ctx.fillStyle = "#808080";
  }
  if (person.state == 3 ) {
    ctx.fillStyle = "#0000FF";
  }
  // ctx.drawImage(person.color, person.xpos, person.ypos, personRadius, personRadius)
  ctx.fill();
  ctx.closePath();
}


function drawGrid() {
  for (let i = 0; i < canvas.width; i+= 10) {
    ctx.beginPath();
    ctx.moveTo(i,0);
    ctx.lineTo(i,canvas.height);
    ctx.closePath();
    ctx.lineWidth = 0.1 ;
    ctx.stroke();
  }
  for (let i = 0; i < canvas.height; i+= 10) {
    ctx.beginPath();
    ctx.moveTo(0,i);
    ctx.lineTo(canvas.width,i);
    ctx.closePath();
    ctx.lineWidth = 0.1 ;
    ctx.stroke();
  }
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  // drawGrid();

  for (let person of population) {
    drawPerson(person);
    avoidWalls(person);
    avoidCollision(person);

    if( person.state == 0) {
      //Contamination
      for (let virus of infected) {
        dx = virus.xpos - person.xpos ;
        dy = virus.ypos - person.ypos ; // LOL the error was that i've put = instead of -
        distance = Math.sqrt(dx*dx + dy*dy) ;
        if ( distance < 2*personRadius && infectionProb() ) {
          person.isInfected() ;
          healthy.delete(person) ;
          infected.add(person) ;
        }
      }
    }
    else if(person.state == 1){ //Death Prob of infected people
      if( dyingProb() ) {
        person.isDead()  ;
        infected.delete(person) ;
        dead.add(person) ;
      }
      else if(recoveryProb()) {
        person.isRecovered() ;
        infected.delete(person);
        recovered.add(person);
      }
    }
      
    person.setX(person.position.x + person.direction.x) ;
    person.setY(person.position.y + person.direction.y) ;
    
    
    updateScore();
  }
}



const resetAll = document.getElementById("newValue").onclick = function () {
  resetGame() ;
}

function makeInterval(){
  const intervalId = setInterval(draw,40);
  return intervalId ; 
}
let intervalId = makeInterval();

function resetGame() {
  let useEmojy = false ;
  
  numberOfPeople  = parseInt(document.getElementById("popul").value     ) ; 
  infectprob      = parseInt(document.getElementById("infecProb").value ) ;
  dieprob         = parseInt(document.getElementById("dyingProb").value ) ;
  recovprob       = parseInt(document.getElementById("recovProb").value ) ;
  personRadius    = parseInt(document.getElementById("ballRadius").value) ;
  
  useEmojy = document.querySelector('#toggleEmojy:checked') !== null ? true : false

  if (useEmojy) {
    // emojy pngs
    imgHealhty.src = "/js/game/img/smiling.png";
    imgInfected.src = "/js/game/img/virus.png";
    imgrecovered.src = "/js/game/img/recovered2.png";
    imgDead.src = "/js/game/img/dead2.png";
  }
  else {
    // colors pngs
    imgHealhty.src = "/js/game/img/green.png";
    imgInfected.src = "/js/game/img/red.png";
    imgrecovered.src = "/js/game/img/blue.png";
    imgDead.src = "/js/game/img/gray.png";
  }
  
  


  population = [] ;
  infected = new Set() ;
  healthy = new Set() ;
  dead = new Set() ;
  recovered = new Set();

  createPopulation()
  
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  clearInterval(intervalId) ;
  intervalId = makeInterval();

}




//***************************************************************************


















